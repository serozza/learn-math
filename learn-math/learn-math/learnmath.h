#ifndef LEARNMATH_H
#define LEARNMATH_H

#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <cmath>

namespace learnmath {

static const int firstPrime = 2;

inline void print(std::vector<int> vec)
{
    for(const auto& item : vec)
        std::cout << item << " ";
}


bool isPrime(const int n)
{
    bool isPrime = true;
    for(int i = n - 1; i >= firstPrime; --i)
    {
        if((n % i) == 0)
        {
            isPrime = false;
            break;
        }
    }

    return isPrime;
}

std::vector<int> getPrimeNumbersTill(const int n)
{
    std::vector<int> result;
    int currentNum = firstPrime;
    while (true)
    {
        if(currentNum > n)
            break;

        if(isPrime(currentNum))
            result.push_back(currentNum);

        ++currentNum;
    }

    return result;
}

void printPrimeNumbersTill(const int n)
{
    print(getPrimeNumbersTill(n));
}

std::vector<int> splitToPrime(int value)
{
    std::vector<int> primes = getPrimeNumbersTill(value);
    std::vector<int> result;
    for(auto i = primes.begin(); i != primes.end();)
    {
        const int minimalPrime = *i;
        if(isPrime(value))
        {
            result.push_back(value);
            break;
        }

        if((value % minimalPrime) == 0)
        {
            value /= minimalPrime;
            result.push_back(minimalPrime);
        }
        else
        {
            ++i;
        }
    }

    return result;
}

void factorize(int value)
{
    auto primes = splitToPrime(isPrime(value) ? value - 1 : value);

    std::cout << value << " = ";
    std::cout << "( ";
    for(int i = 0; i < primes.size(); ++i)
    {
        std::cout << primes[i];
        if(i != primes.size() - 1)
            std::cout << "*";
    }
    std::cout << " )";

    int diff = value - std::accumulate(primes.begin(), primes.end(), 1, std::multiplies<int>());
    if(diff)
        std::cout << " + " << diff;

    std::cout << std::endl;
}

}
#endif // LEARNMATH_H
